%bcond_without mesa_glvnd_default
%if %{without mesa_glvnd_default}
%global __provides_exclude_from %{_libdir}/%{name}
%global __requires_exclude_from %{_libdir}/%{name}
%endif

Summary:        The GL Vendor-Neutral Dispatch library
Name:           libglvnd
Version:        1.6.0
Release:        1%{?dist}
License:        MIT
URL:            https://gitlab.freedesktop.org/glvnd/libglvnd
Source0:        %{url}/-/archive/v%{version}/%{name}-v%{version}.tar.gz
Patch3000:      0001-glx-Add-another-fallback-library-name.patch

BuildRequires:	make gcc libtool
BuildRequires:  python3-rpm-macros python3-libxml2
BuildRequires:  pkgconfig(glproto) pkgconfig(x11) pkgconfig(xext)
BuildRequires:  xorg-x11-server-Xvfb

%description
libglvnd is a vendor-neutral dispatch layer for arbitrating OpenGL API calls
between multiple vendors. It allows multiple drivers from different vendors to
coexist on the same filesystem, and determines which vendor to dispatch each
API call to at runtime.

%package        opengl
Summary:        OpenGL support for libglvnd
Requires:       %{name} = %{version}-%{release}

%description    opengl
libOpenGL is the common dispatch interface for the workstation OpenGL API.

%package        gles
Summary:        GLES support for libglvnd
Requires:       %{name} = %{version}-%{release}
%if %{with mesa_glvnd_default}
Requires:       mesa-libEGL >= 13.0.4-1
Provides:       mesa-libGLES
Provides:       libGLES
%endif

%description    gles
libGLESv[12] are the common dispatch interface for the GLES API.

%package        egl
Summary:        EGL support for libglvnd
Requires:       %{name} = %{version}-%{release}
%if %{with mesa_glvnd_default}
Requires:       mesa-libEGL >= 13.0.4-1
Provides:       libEGL
%endif

%description    egl
libEGL are the common dispatch interface for the EGL API.

%package        glx
Summary:        GLX support for libglvnd
Requires:       %{name} = %{version}-%{release}
%if %{with mesa_glvnd_default}
Requires:       mesa-libGL >= 13.0.4-1
Provides:       libGL
%endif

%description    glx
libGL and libGLX are the common dispatch interface for the GLX API.

%package        devel
Summary:        Header files and libraries for %{name} development
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-opengl = %{version}-%{release}
Requires:       %{name}-gles = %{version}-%{release}
Requires:       %{name}-glx = %{version}-%{release}
Requires:       %{name}-egl = %{version}-%{release}
Requires:       libX11-devel
Provides:       mesa-libGLES-devel = %{version}-%{release}
Provides:       mesa-khr-devel = %{version}-%{release}
Provides:       libGLES-devel = %{version}-%{release}

%description    devel
This package provides header files and libraries for %{name} development.

%prep
%autosetup -p1 -n %{name}-v%{version}
autoreconf -vif

%build
export PYTHON=%{__python3}
%configure \
  --disable-static \
  --enable-asm \
  --enable-tls

%make_build V=1

%install
%make_install INSTALL="install -p"
find %{buildroot} -name '*.la' -delete

%if %{without mesa_glvnd_default}
mkdir -p %{buildroot}%{_libdir}/%{name}/
for l in libEGL libGL libGLESv1_CM libGLESv2 libGLX; do
  mv %{buildroot}%{_libdir}/${l}.so* \
    %{buildroot}%{_libdir}/%{name}/
done
%endif

mkdir -p %{buildroot}%{_sysconfdir}/glvnd/egl_vendor.d/
mkdir -p %{buildroot}%{_datadir}/glvnd/egl_vendor.d/
mkdir -p %{buildroot}%{_sysconfdir}/egl/egl_external_platform.d/
mkdir -p %{buildroot}%{_datadir}/egl/egl_external_platform.d/

%check
# disable aarch64 temporarily
%ifnarch aarch64
export DO_X11_TESTS=1
export LD_LIBRARY_PATH=%{_libdir}/libglvnd
xvfb-run -s '-screen 0 640x480x24' -d make check V=1 || \
(cat `find . -name test-suite.log` ; exit 1)
%endif

%files
%doc README.md
%dir %{_sysconfdir}/glvnd/
%dir %{_datadir}/glvnd/
%{_libdir}/libGLdispatch.so.0*

%files opengl
%{_libdir}/libOpenGL.so.0*

%files gles
%if %{without mesa_glvnd_default}
%{_libdir}/%{name}/libGLES*.so.*
%else
%{_libdir}/libGLES*.so.*
%endif

%files egl
%dir %{_sysconfdir}/glvnd/egl_vendor.d/
%dir %{_datadir}/glvnd/egl_vendor.d/
%dir %{_sysconfdir}/egl/
%dir %{_sysconfdir}/egl/egl_external_platform.d/
%dir %{_datadir}/egl/
%dir %{_datadir}/egl/egl_external_platform.d/
%if %{without mesa_glvnd_default}
%{_libdir}/%{name}/libEGL*.so.*
%else
%{_libdir}/libEGL*.so.*
%endif

%files glx
%if %{without mesa_glvnd_default}
%{_libdir}/%{name}/libGL.so.*
%{_libdir}/%{name}/libGLX.so.*
%else
%{_libdir}/libGL.so.*
%{_libdir}/libGLX.so.*
%endif

%files devel
%dir %{_includedir}/glvnd/
%dir %{_includedir}/EGL/
%dir %{_includedir}/GL/
%dir %{_includedir}/GLES/
%dir %{_includedir}/GLES2/
%dir %{_includedir}/GLES3/
%dir %{_includedir}/KHR/
%{_includedir}/glvnd/*.h
%{_includedir}/EGL/*.h
%{_includedir}/GL/*.h
%{_includedir}/GLES/*.h
%{_includedir}/GLES2/*.h
%{_includedir}/GLES3/*.h
%{_includedir}/KHR/*.h
%{_libdir}/lib*.so
%if %{without mesa_glvnd_default}
%{_libdir}/%{name}/lib*.so
%endif
%{_libdir}/pkgconfig/libglvnd.pc
%{_libdir}/pkgconfig/gl*.pc
%{_libdir}/pkgconfig/egl.pc
%{_libdir}/pkgconfig/opengl.pc

%changelog
* Mon Jul 31 2023 rockerzhu <rockerzhu@tencent.com> - 1.6.0-1
- Update to 1.60.0.

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.5.0-5
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.5.0-4
- Rebuilt for OpenCloudOS Stream 23

* Tue Nov 15 2022 Xiaojie Chen <jackxjchen@tencent.com> - 1.5.0-3
- enable mesa_glvnd_default

* Wed Nov 9 2022 Shuo Wang <abushwang@tencent.com> - 1.5.0-2
- add branch for testsuite

* Mon Sep 19 2022 Xiaojie Chen <jackxjchen@tencent.com> - 1.5.0-1
- Initial build
